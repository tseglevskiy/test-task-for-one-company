package com.tseglevskiy.testtest;

import java.io.IOException;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	final static String TAG = "TestTest";
	final static String whereIsYulia = "https://twitter.com/YuliaTymoshenko";

	
	Button sendSmsButton = null;
	EditText sendSmsPhone = null;
	EditText sendSmsText = null;
	Button getYuliaButton = null;
	TextView getYuliaText = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		/* 
		 * part 1. send SMS
		 */
		sendSmsButton = (Button)findViewById(R.id.sendSmsButton);
		sendSmsButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				sendSms();
			}
		});
		
		sendSmsPhone = (EditText)findViewById(R.id.sendSmsPhone);
		sendSmsText = (EditText)findViewById(R.id.sendSmsText);
		
		/*
		 * part 4. get data over HTTPS
		 */
		
		getYuliaButton = (Button)findViewById(R.id.getYuliaButton);
		getYuliaButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				getYulia();
			}
		});
		
		getYuliaText = (TextView)findViewById(R.id.getYuliaText);
		
		/*
		 * part 2. print to log from service every second
		 */
		startService(new Intent(this, OneSecondService.class));
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {			

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	/* ----------------------------------------------------- 
	 * part 5. save activity state after rotation 
	 */

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putString("phone", sendSmsPhone.getText().toString());
		outState.putString("text", sendSmsText.getText().toString());
		outState.putString("yulia", getYuliaText.getText().toString());
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		String str;

		str = savedInstanceState.getString("phone");
		if (str != null) {
			sendSmsPhone.setText(str);
		}

		str = savedInstanceState.getString("text");
		if (str != null) {
			sendSmsText.setText(str);
		}

		str = savedInstanceState.getString("yulia");
		if (str != null) {
			getYuliaText.setText(str);
		}

	}
	
	/* ----------------------------------------------------- */
	
	/* 
	 * send SMS with reflection
	 */
	void sendSms() {
		Log.d(TAG, "sendSms");
		
		String phone = sendSmsPhone.getText().toString();
		String text = sendSmsText.getText().toString();
		if (phone.length() == 0 || text.length() == 0) {
			return;
		}
		
		WrapSmsManager sms = new WrapSmsManager();
		sms.sendTextMessage(phone, null, text, null, null);
		
	}
	
	/*
	 * get data over HTTPS
	 */
	void getYulia() {
		new Thread() {
			@Override
			public void run() {
				try {
					HttpClient client = new DefaultHttpClient();
					HttpGet request = new HttpGet(whereIsYulia);
					HttpResponse response = client.execute(request);
					String html = EntityUtils.toString(response.getEntity());
					
					Log.d(TAG, "got " + html.length() + " characters about Yulia");
					showYulia(html.length());
					
				} catch (ClientProtocolException e) {
					Log.d(TAG, e.getMessage());
				} catch (IOException e) {
					Log.d(TAG, e.getMessage());
				}
			}
		}.start();
	}
	
	/*
	 * show results in UI
	 */
	void showYulia(final int len) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				getYuliaText.setText("got " + len + " characters");

			}
		});
	}
}
