package com.tseglevskiy.testtest;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

public class OneSecondService extends Service {
	protected static final int period = 1000; // 1000ms = 1s
	protected static final String TAG = "TestTest";
	
	private static int counter = 1;
	
	private Handler mHandler = null;
	private Runnable worker = null;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mHandler = new Handler();
		worker = new Runnable() {
			@Override
			public void run() {
				printValue();

				mHandler.removeCallbacks(this);
				if (this == worker) {
					// system does not provide guarantees that worker will run again in 'period' milliseconds.
					mHandler.postDelayed(worker, period);  
				}
			}

		};
		mHandler.postDelayed(worker, period); // first run
		
		Log.d(TAG, "Service started");
	}

	@Override
	public void onDestroy() {
		if (mHandler != null && worker != null) {
			mHandler.removeCallbacks(worker);
		}
		worker = null;
		mHandler = null;
		
		Log.d(TAG, "Service stoped");
		
		super.onDestroy();
	}
	
	private void printValue() {
		Log.d(TAG, "count: " + counter++); // 'counter' will reset when this service restarts
	}
}
