package com.tseglevskiy.testtest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class SmsListener extends BroadcastReceiver {

	private static final String TAG = "TestTest";

	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d(TAG, "SmsListener.onReceive");
		Bundle bundle = intent.getExtras();
		if (bundle == null) {
			return;
		}

		try{
			Object[] pdus = (Object[]) bundle.get("pdus");
			for(int i=0; i<pdus.length; i++){
				WrapSmsMessage msg = WrapSmsMessage.createFromPdu((byte[])pdus[i]);
				String msgFrom = msg.getOriginatingAddress();
				String msgBody = msg.getMessageBody();
				Log.d(TAG, "message " + i + " from " + msgFrom + " '" + msgBody + "'");
			}
		
			// here I can test some conditions
			abortBroadcast();
			
		}catch(Exception e){
			Log.d(TAG, e.getMessage());
		}
	}

}
