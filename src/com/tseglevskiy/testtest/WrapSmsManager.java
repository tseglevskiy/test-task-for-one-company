package com.tseglevskiy.testtest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.app.PendingIntent;
import android.util.Log;


public class WrapSmsManager {
	private static final String TAG = "TestTest";

	private static Class<?> originalClass = null;
	private static Object smsManager = null;
	private static Method sendSmsMethod = null;

	static {
		initCompatibility();
	}

	@SuppressWarnings("deprecation")
	private static void initCompatibility() {
		try {
			originalClass = Class.forName("android.telephony.SmsManager");
			smsManager = android.telephony.SmsManager.getDefault();
			sendSmsMethod = originalClass.getMethod(
					"sendTextMessage", 
					new Class[] { String.class, String.class, String.class, 
							PendingIntent.class, PendingIntent.class } 
					);
			Log.d(TAG, "API 4+");
		} catch (Exception ex) {
			try {
				originalClass = Class.forName("android.telephony.gsm.SmsManager");
				smsManager = android.telephony.gsm.SmsManager.getDefault();
				sendSmsMethod = originalClass.getMethod(
						"sendTextMessage", 
						new Class[] { String.class, String.class, String.class, 
								PendingIntent.class, PendingIntent.class } 
						);
				Log.d(TAG, "API 1..3");
			} catch (Exception exx) {
				Log.d(TAG, "it is impossible, but do nothing");
				
			}
		}

	}
	
	public WrapSmsManager() {
		
	}
	
	public void sendTextMessage (String destinationAddress, String scAddress, String text, 
			PendingIntent sentIntent, PendingIntent deliveryIntent) 
	{
		try {
			if (sendSmsMethod != null && smsManager != null) {
				sendSmsMethod.invoke(smsManager, destinationAddress, scAddress, text, 
						sentIntent, deliveryIntent);
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
		} catch (InvocationTargetException e) {
			Log.d(TAG, e.getMessage());
		}
			
	}

}
