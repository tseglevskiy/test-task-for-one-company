package com.tseglevskiy.testtest;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.util.Log;

public class WrapSmsMessage {
	private static final String TAG = "TestTest";
	
	private static Class<?> originalClass = null;
	private static Method createFromPduMethod = null;
	private static Method getOriginatingAddressMethod = null;
	private static Method getMessageBodyMethod = null;
	
	private Object mInstance = null;

	static {
		initCompatibility();
	}

	private static void initCompatibility() {
		try {
			originalClass = Class.forName("android.telephony.SmsMessage");
			createFromPduMethod = originalClass.getMethod("createFromPdu", new Class[] { byte[].class });
			getOriginatingAddressMethod = originalClass.getMethod("getOriginatingAddress", new Class[] { });
			getMessageBodyMethod = originalClass.getMethod("getMessageBody", new Class[] { });

			Log.d(TAG, "API 4+");
		} catch (Exception e) {
			try {
				originalClass = Class.forName("android.telephony.gsm.SmsMessage");
				createFromPduMethod = originalClass.getMethod("createFromPdu", new Class[] { byte[].class });
				getOriginatingAddressMethod = originalClass.getMethod("getOriginatingAddress", new Class[] { });
				getMessageBodyMethod = originalClass.getMethod("getMessageBody", new Class[] { });

				Log.d(TAG, "API 1..3");
			} catch (Exception ee) {
				Log.d(TAG, "it is impossible, but do nothing ");
			}
		}
	}
	
	public WrapSmsMessage(Object sms) {
		mInstance = sms;
	}

	public static WrapSmsMessage createFromPdu (byte[] pdu) 
	{
		try {
			if (createFromPduMethod != null) {
				Object sms = createFromPduMethod.invoke(null, pdu);
				return new WrapSmsMessage(sms);
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
		} catch (InvocationTargetException e) {
			Log.d(TAG, e.getMessage());
		}
		return null;
	}

	public String getOriginatingAddress () 
	{
		try {
			if (getOriginatingAddressMethod != null) {
				return (String)getOriginatingAddressMethod.invoke(mInstance);
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
		} catch (InvocationTargetException e) {
			Log.d(TAG, e.getMessage());
		}
		return "";
	}
	
	public String getMessageBody () 
	{
		try {
			if (getMessageBodyMethod != null) {
				return (String)getMessageBodyMethod.invoke(mInstance);
			}
		} catch (IllegalArgumentException e) {
			Log.d(TAG, e.getMessage());
		} catch (IllegalAccessException e) {
			Log.d(TAG, e.getMessage());
		} catch (InvocationTargetException e) {
			Log.d(TAG, e.getMessage());
		}
		return "";
	}
}
